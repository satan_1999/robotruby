# robot.rb

class Robot
  @@maxPosition = 5
  @@faces= ["EAST","NORTH","WEST","SOUTH"]
  @@commands =["PLACE","MOVE","LEFT","RIGHT","REPORT"]
  attr_accessor :xPosition
  attr_accessor :yPosition
  attr_accessor :face

  #Read string command
  def readCommand( command )

    case command.strip.upcase
    when /\APLACE/
      command = command.strip.upcase.gsub(@@commands[0], "")
      params = command.split(",")
      place(params[0].strip.to_i, params[1].strip.to_i, params[2].strip)
      return
    when  @@commands[1]
      move
      return
    when @@commands[2]
      left
      return
    when @@commands[3]
      right
      return
    when @@commands[4]
      return report
    else
      raise "Illegal Command >>#{command}"
    end

  end

  #report robot status
  def report
    puts "Current Location X[#{@xPosition}] Y[#{@yPosition}] Face[#{@face}]"
    return "Current Location X[#{@xPosition}] Y[#{@yPosition}] Face[#{@face}]"
  end

  #place robot
  def place(xPosition, yPosition, face)
    if validateRobotPlace(xPosition, yPosition, face)
      @xPosition = xPosition
      @yPosition = yPosition
      @face = face
    else
      raise 'Illegal Arguments'
    end
  end

  def validateRobotPlace(xPosition, yPosition, face)
    #the x/y position must be between 0 and maxPosition
    #face must be one of value in @@faces
    if xPosition && xPosition.between?(0, @@maxPosition) && yPosition && yPosition.between?(0, @@maxPosition) && face && @@faces.include?(face)
      return true
    else
      puts xPosition, yPosition, face
      return false
    end
  end

  #turn left
  def left
    if !validateRobotPlace(@xPosition, @yPosition, @face)
      puts "Please place robot first"
      return
    end
    index = @@faces.find_index(@face.upcase)

    if index
      index = (index < (@@faces.size - 1)) ? index + 1 : 0
      @face =  @@faces[index]
    end
  end

  #turn right
  def right
    if !validateRobotPlace(@xPosition, @yPosition, @face)
      puts "Please place robot first"
      return
    end

    index = @@faces.find_index(@face.upcase)

    if index
      @face =  @@faces[index -1]
    end
  end

  #move further
  def move
    if !validateRobotPlace(@xPosition, @yPosition, @face)
      puts "Please place robot first"
      return
    end

    case @face.upcase
    when  @@faces[0]
      if (@xPosition + 1) <= @@maxPosition
        @xPosition +=1
        puts "Moving East"
      else
        puts "Invalid Move"
      end
    when   @@faces[2]
      if (@xPosition - 1) >=0
        @xPosition -=1
        puts "Moving West"
      else
        puts "Invalid Move"
      end
    when   @@faces[3]
      if (@yPosition - 1) >=0
        @yPosition -=1
        puts "Moving South"
      else
        puts "Invalid Move"
      end
    when   @@faces[1]
      if (@yPosition + 1) <= @@maxPosition
        @yPosition +=1
        puts "Moving North"
      else
        puts "Invalid Move"
      end
    else
      puts "Invalid Direction"
    end
  end
end