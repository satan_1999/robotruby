# robotDemoReadFile.rb
require_relative 'Robot'

puts "Robot Demo -- Read Command From File"

def readCommands
  commands = []
  File.open('./source/commands.txt', 'r') do |f1|
    while line = f1.gets
      commands << line
    end
  end
  puts "Read #{commands.size()} Commands from file"
  return commands
end

def demo
  puts "Start demo ..."
  robot = Robot.new
  readCommands.each {
    |command| robot.readCommand(command.strip)
  }
  puts "End of demo ..."
end

demo

