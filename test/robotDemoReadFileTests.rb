require "./source/robot"
require "./source/robotDemoReadFile"
require "test/unit"

class RobotDemoReadFileTests < Test::Unit::TestCase

  def test_readCommands
    assert_equal(6, readCommands.size)
  end
  
  def test_demo
      assert_nothing_raised do
        demo
      end
    end

end