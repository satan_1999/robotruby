require "./source/robot"
require "test/unit"
require "shoulda"

class RobotTests_Shoulda < Test::Unit::TestCase

  context "Robot" do

    should "take string command place 2,2,NORTH" do
      robot = Robot.new()
      robot.readCommand('place 2,2,NORTH')
      assert_equal 2, robot.xPosition
      assert_equal 2, robot.yPosition
      assert_equal "NORTH", robot.face
    end

    should "take string command move" do
      robot = Robot.new()
      robot.place(2,2,"NORTH")
      robot.readCommand('move')
      assert_equal 2, robot.xPosition
      assert_equal 3, robot.yPosition
      assert_equal "NORTH", robot.face
    end

    should "take string command right" do
      robot = Robot.new()
      robot.place(2,2,"NORTH")
      robot.readCommand('right')
      assert_equal 2, robot.xPosition
      assert_equal 2, robot.yPosition
      assert_equal "EAST", robot.face
    end

    should "take string command left" do
      robot = Robot.new()
      robot.place(2,2,"NORTH")
      robot.readCommand('left')
      assert_equal 2, robot.xPosition
      assert_equal 2, robot.yPosition
      assert_equal "WEST", robot.face
    end

    should "take string command report" do
      robot = Robot.new()
      robot.place(2,2,"NORTH")
      output = robot.readCommand('report')
      assert_equal "Current Location X[2] Y[2] Face[NORTH]", output

    end

    should "place robot to X[2] Y[2] Face[NORTH]" do
      robot = Robot.new()
      robot.place(2,2,"NORTH")
      assert_equal 2, robot.xPosition
      assert_equal 2, robot.yPosition
      assert_equal "NORTH", robot.face
    end

    should "expect error :  Illegal Command" do
      robot = Robot.new()
      assert_raise( RuntimeError ) { robot.readCommand('eee') }
    end

    should "expect error from place , due y=6" do
      robot = Robot.new()
      assert_raise( RuntimeError ) { robot.place(2,6,"NORTH") }

    end

    should "expect false from validateRobotPlace , due x=-1" do
      robot = Robot.new()
      assert_equal( false, robot.validateRobotPlace(-1,2,"NORTH") )

    end

    should "expect false from validateRobotPlace , due face = xxx" do
      robot = Robot.new()
      assert_equal( false, robot.validateRobotPlace(2,3,"xxx") )

    end

    should "expect true from validateRobotPlace , due x in [0..5]" do
      robot = Robot.new()
      [0,5].each{ |x|
        assert_equal( true, robot.validateRobotPlace(x,3,"NORTH") )
      }

    end

    should "expect true from validateRobotPlace , due y in [0..5]" do
      robot = Robot.new()
      [0,5].each{ |y|
        assert_equal( true, robot.validateRobotPlace(1,y,"NORTH") )
      }

    end

    should "expect true from validateRobotPlace , due face in [EAST,NORTH,WEST,SOUTH]" do
      robot = Robot.new()
      ["EAST","NORTH","WEST","SOUTH"].each{ |face|
        assert_equal( true, robot.validateRobotPlace(1,0,face) )
      }

    end

    should "turn right, expect correct direction" do
      robot = Robot.new()
      robot.place(2,1,"NORTH")
      directions = ["EAST","SOUTH","WEST","NORTH","EAST","SOUTH","WEST","NORTH"]
      directions.each_with_index{ |face, index|
        robot.right
        assert_equal(directions[index], robot.face)
      }

    end

    should "turn left, expect correct direction" do
      robot = Robot.new()
      robot.place(2,1,"SOUTH")
      directions = ["EAST","NORTH","WEST","SOUTH","EAST","NORTH","WEST","SOUTH"]
      directions.each_with_index{ |face, index|
        puts "turn left from #{robot.face} #{index}"
        robot.left
        assert_equal(directions[index], robot.face)
      }

    end

    should "move to X[0] Y[5] Face[NORTH]" do
      robot = Robot.new()
      robot.place(0,0,"NORTH")
      Array.new(7){ |x|	robot.move		 }
      assert_equal 0, robot.xPosition
      assert_equal 5, robot.yPosition
      assert_equal "NORTH", robot.face

    end

    should "move to X[5] Y[0] Face[NORTH]" do
      robot = Robot.new()
      robot.place(0,0,"EAST")
      Array.new(7){ |x|	robot.move		 }
      assert_equal 5, robot.xPosition
      assert_equal 0, robot.yPosition
      assert_equal "EAST", robot.face

    end

    should "move from X[5] to X[0] " do
      robot = Robot.new()
      robot.place(5,0,"WEST")
      Array.new(7){ |x|	robot.move		 }
      assert_equal 0, robot.xPosition
    end

    should "move from Y[5] to Y[0] " do
      robot = Robot.new()
      robot.place(0,5,"SOUTH")
      Array.new(7){ |x|	robot.move		 }
      assert_equal 0, robot.yPosition
    end

  end

end